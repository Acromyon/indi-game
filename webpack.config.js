const path = require('path');

module.exports = (env, argv) => {
  const isDev = argv.mode === 'development'
  
  return {
    mode: argv.mode,
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'public'),
      filename: 'game.bundle.js',
    },
    devtool: isDev ? 'eval-source-map' : false,
    devServer: {
      static: {
        directory: path.join(__dirname, 'public'),
      },
      compress: true,
      port: 9000,
      open: true,
    },
    module: {
      rules: [
        {
          test: /\.m?js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: [
                ['@babel/preset-env', { targets: "defaults" }]
              ]
            }
          }
        }
      ]
    }
  }
};
